import subprocess as subp
import matplotlib.pyplot as plt
import numpy as np
import re, os

def buildCommand(cacheSize, blockSize, traceFile, numOfLevels=2,
        cacheType="unified", scalingSuffix='k', degreeOfAssociativity=1,
        inputFormat="traditional din", tracesPath="traces/",
        dineroPath="./dineroIV", show3CsStats=True):
    """Creates Dinero IV command.

    Keyword arguments:
    numOfLevels -- number of levels in cache (default 2)
    cacheType -- unified, instruction, data (default "unified")
    cacheSize -- in units of ${scalingSuffix}
    scalingSuffix -- k=KiB, m=MiB, g=GiB (default 'k')
    blockSize -- in bytes
    degreeOfAssociativity -- (default 1)
    inputFormat -- (default "traditional din")
    tracesPath -- path to folder with trace files (default "traces/")
    traceFile -- trace file to use
    dineroPath -- (default "./dineroIV")
    show3CsStats -- show Compulsory/Capacity/Conflict miss statistics (default True)
    """

    # Get cache type character identifier
    if cacheType == "unified":
        T='-u'
    elif cacheType == "instruction":
        T='-i'
    elif cacheType == "data":
        T='-d'
    
    # Get input format type
    if inputFormat == "traditional din":
        informat='d'
    elif inputFormat == "extended din":
        informat='D'
    elif inputFormat == "pixie32":
        informat='p'
    elif inputFormat == "pixie64":
        informat='P'
    elif inputFormat == "binary":
        informat='b'

    command=dineroPath

    # Loop through the levels of cache we will use
    for N in range(numOfLevels-1):
        argStart=" -l"+str(N+1)+T
        size=(argStart+"size "+str(cacheSize)+scalingSuffix)
        bsize=(argStart+"bsize "+str(blockSize))
        assoc=(argStart+"assoc "+str(degreeOfAssociativity))
        
        if show3CsStats == True:
            ccc=(argStart+"ccc ")
            command+=(size+bsize+assoc+ccc)
        else:
            command+=(size+bsize+assoc+" ")

    return command+"-informat "+informat+" < "+tracesPath+traceFile+"\n"

def buildScript1(associativitiesList, fileName="hw1.sh"):
    """Writes multiple Dinero IV invocations to a script file.

    Keyword arguments:
    associativitiesList -- list of associativities to test
    fileName -- file to write to (default "tmp.sh")
    """

    f = open(fileName, "a")
    for degree in associativitiesList:
        f.write(buildCommand(cacheSize=64, scalingSuffix='k', blockSize=8, degreeOfAssociativity=degree, traceFile="mtu-ts.din"))

    f.close()
    return


def buildScript2(associativitiesList, cacheSizeList, traceFileList, fileName="hw2.sh"):
    """Writes multiple Dinero IV invocations to a script file.

    Keyword arguments:
    associativitiesList -- list of associativities to test
    cacheSizeList -- list of cache sizes to test
    traceFileList -- list of trace files to run
    fileName -- file to write to (default "tmp.sh")
    """

    f = open(fileName, "a")
    for trace in traceFileList:
        for degree in associativitiesList:
            for size in cacheSizeList:
                f.write(buildCommand(cacheSize=size, scalingSuffix='k', blockSize=16, degreeOfAssociativity=degree, show3CsStats=False, traceFile=trace))
        
    f.close()
    return


def buildScript3(associativitiesList, cacheSizeList, traceFileList, fileName="hw3.sh"):
    """Writes multiple Dinero IV invocations to a script file.

    Keyword arguments:
    associativitiesList -- list of associativities to test
    cacheSizeList -- list of cache sizes to test
    traceFileList -- list of trace files to run
    fileName -- file to write to (default "tmp.sh")
    """

    f = open(fileName, "a")
    for trace in traceFileList:
        for degree in associativitiesList:
            for size in cacheSizeList:
                f.write(buildCommand(cacheSize=size, scalingSuffix='k', blockSize=16, degreeOfAssociativity=degree, traceFile=trace))
        
    f.close()
    return

# def buildScript4(associativitiesList, cacheSizeList, traceFileList, fileName="hw4.sh"):
#     """Writes multiple Dinero IV invocations to a script file.

#     Keyword arguments:
#     associativitiesList -- list of associativities to test
#     cacheSizeList -- list of cache sizes to test
#     traceFileList -- list of trace files to run
#     fileName -- file to write to (default "tmp.sh")
#     """

#     f = open(fileName, "a")
#     for trace in traceFileList:
#         for degree in associativitiesList:
#             for size in cacheSizeList:
#                 f.write(buildCommand(cacheSize=size, scalingSuffix='k', blockSize=16, numOfLevels=3, degreeOfAssociativity=degree, traceFile=trace))
        
#     f.close()
#     return

## HW 1
associativitiesList=[1,2,4,8]
associativitiesLabels=["1","2","4","8"]
buildScript1(associativitiesList)
result=subp.run(['bash', 'hw1.sh'], stdout=subp.PIPE)
#os.remove("hw1.sh")
match=re.findall('Demand miss rate[ \t]+(\d.\d+)', result.stdout.decode())
if match:
    hitRates=[1-float(x) for x in match]
print(hitRates)
print("\n")
# [0.9917, 0.9932, 0.9934, 0.9935]
# plt.bar(associativitiesLabels, hitRates)
# plt.title("Hit rates of caches with varing degrees of associativity")
# plt.xlabel("Degree of Associativity")
# plt.ylabel("Hit Rate")
# plt.show()

## HW 2
associativitiesList=[1,2]
cacheSizeList=[1,2,4,8,16,32,64,128]
traceFileList=["cc1.din","trace64.din"]
associativitiesLabels=["1","2"]
associativitiesLabels=["1K","2K","4K","8K","16K","32K","64K","128K"]
buildScript2(associativitiesList, cacheSizeList, traceFileList)
result=subp.run(['bash', 'hw2.sh'], stdout=subp.PIPE)
#os.remove("hw2.sh")
match=re.findall('Demand miss rate[ \t]+(\d.\d+)', result.stdout.decode())
if match:
    hitRates=[float(x) for x in match]
print(hitRates)
print("\n")
# Take all but first value of each file set to keep things in order
# [0.2312, 0.2077, 0.1861, 0.1646, 0.1409, 0.1217, 0.1012, 0.0823, 0.0634, 
# 0.0478, 0.0381, 0.0263, 0.0271, 0.018, 0.0195, 0.0151, 0.3458, 0.261, 
# 0.3207, 0.2227, 0.3079, 0.2164, 0.3009, 0.2138, 0.2787, 0.211, 0.2614, 
# 0.1971, 0.2525, 0.1962, 0.202, 0.1956]
# so...
# [NULL, 0.2077, 0.1861, 0.1646, 0.1409, 0.1217, 0.1012, 0.0823, 0.0634, 
# 0.0478, 0.0381, 0.0263, 0.0271, 0.018, 0.0195, 0.0151, NULL, 0.261, 
# 0.3207, 0.2227, 0.3079, 0.2164, 0.3009, 0.2138, 0.2787, 0.211, 0.2614, 
# 0.1971, 0.2525, 0.1962, 0.202, 0.1956]
# plt.bar(associativitiesLabels, hitRates)
# plt.title("Hit rates of caches with varing degrees of associativity")
# plt.xlabel("Degree of Associativity")
# plt.ylabel("Hit Rate")
# plt.show()

## HW 3
associativitiesList=[1,2]
cacheSizeList=[1,2,4,8,16,32,64,128]
traceFileList=["cc1.din","trace64.din"]
associativitiesLabels=["1","2"]
associativitiesLabels=["1K","2K","4K","8K","16K","32K","64K","128K"]
buildScript3(associativitiesList, cacheSizeList, traceFileList)
result=subp.run(['bash', 'hw3.sh'], stdout=subp.PIPE)
#os.remove("hw3.sh")
compulsory=re.findall('Compulsory fraction[ \t]+(\d.\d+)', result.stdout.decode())
capacity=re.findall('Capacity fraction[ \t]+(\d.\d+)', result.stdout.decode())
conflict=re.findall('Conflict fraction[ \t]+(\d.\d+)', result.stdout.decode())
if compulsory and capacity and conflict:
    compulsoryFraction=[float(x) for x in compulsory]
    capacityFraction=[float(x) for x in capacity]
    conflictFraction=[float(x) for x in conflict]
print(compulsoryFraction)
print("\n")
print(capacityFraction)
print("\n")
print(conflictFraction)

## HW4
# associativitiesList=[1,2]
# cacheSizeList=[1,2,4,8,16,32,64,128]
# traceFileList=["mtu-all.din"]
# associativitiesLabels=["1","2"]
# associativitiesLabels=["1K","2K","4K","8K","16K","32K","64K","128K"]
# buildScript3(associativitiesList, cacheSizeList, traceFileList)
# result=subp.run(['bash', 'hw4.sh'], stdout=subp.PIPE)
# #os.remove("hw4.sh")
# compulsory=re.findall('Compulsory fraction[ \t]+(\d.\d+)', result.stdout.decode())
# capacity=re.findall('Capacity fraction[ \t]+(\d.\d+)', result.stdout.decode())
# conflict=re.findall('Conflict fraction[ \t]+(\d.\d+)', result.stdout.decode())
# if compulsory and capacity and conflict:
#     compulsoryFraction=[float(x) for x in compulsory]
#     capacityFraction=[float(x) for x in capacity]
#     conflictFraction=[float(x) for x in conflict]
# print(compulsoryFraction)
# print("\n")
# print(capacityFraction)