Name: Wilson Holmes
Email: wilsonh@mtu.edu
Date: October 7th, 2021

EE 4173 Homework 3
==================

## 1.

![](q1.png)

DineroIV commands/script:
```sh
./dineroIV -l1-usize 64k -l1-ubsize 8 -l1-uassoc 1 -l1-uccc -informat d < traces/mtu-ts.din
./dineroIV -l1-usize 64k -l1-ubsize 8 -l1-uassoc 2 -l1-uccc -informat d < traces/mtu-ts.din
./dineroIV -l1-usize 64k -l1-ubsize 8 -l1-uassoc 4 -l1-uccc -informat d < traces/mtu-ts.din
./dineroIV -l1-usize 64k -l1-ubsize 8 -l1-uassoc 8 -l1-uccc -informat d < traces/mtu-ts.din
```

## 2.

### (a)

I would say that the `2:1` equivalency holds. They are very close to one another.

![](q2-a.png)

### (b)

I is close again, I am not sure how close it needs to be to cut it, but I think it is close enough.

![](q2-b.png)

### (c)

![](q2-1.png)
![](q2-2.png)

DineroIV commands/script:
```sh
./dineroIV -l1-usize 1k -l1-ubsize 16 -l1-uassoc 1 -informat d < traces/cc1.din
./dineroIV -l1-usize 2k -l1-ubsize 16 -l1-uassoc 1 -informat d < traces/cc1.din
./dineroIV -l1-usize 4k -l1-ubsize 16 -l1-uassoc 1 -informat d < traces/cc1.din
./dineroIV -l1-usize 8k -l1-ubsize 16 -l1-uassoc 1 -informat d < traces/cc1.din
./dineroIV -l1-usize 16k -l1-ubsize 16 -l1-uassoc 1 -informat d < traces/cc1.din
./dineroIV -l1-usize 32k -l1-ubsize 16 -l1-uassoc 1 -informat d < traces/cc1.din
./dineroIV -l1-usize 64k -l1-ubsize 16 -l1-uassoc 1 -informat d < traces/cc1.din
./dineroIV -l1-usize 128k -l1-ubsize 16 -l1-uassoc 1 -informat d < traces/cc1.din
./dineroIV -l1-usize 1k -l1-ubsize 16 -l1-uassoc 2 -informat d < traces/cc1.din
./dineroIV -l1-usize 2k -l1-ubsize 16 -l1-uassoc 2 -informat d < traces/cc1.din
./dineroIV -l1-usize 4k -l1-ubsize 16 -l1-uassoc 2 -informat d < traces/cc1.din
./dineroIV -l1-usize 8k -l1-ubsize 16 -l1-uassoc 2 -informat d < traces/cc1.din
./dineroIV -l1-usize 16k -l1-ubsize 16 -l1-uassoc 2 -informat d < traces/cc1.din
./dineroIV -l1-usize 32k -l1-ubsize 16 -l1-uassoc 2 -informat d < traces/cc1.din
./dineroIV -l1-usize 64k -l1-ubsize 16 -l1-uassoc 2 -informat d < traces/cc1.din
./dineroIV -l1-usize 128k -l1-ubsize 16 -l1-uassoc 2 -informat d < traces/cc1.din
./dineroIV -l1-usize 1k -l1-ubsize 16 -l1-uassoc 1 -informat d < traces/trace64.din
./dineroIV -l1-usize 2k -l1-ubsize 16 -l1-uassoc 1 -informat d < traces/trace64.din
./dineroIV -l1-usize 4k -l1-ubsize 16 -l1-uassoc 1 -informat d < traces/trace64.din
./dineroIV -l1-usize 8k -l1-ubsize 16 -l1-uassoc 1 -informat d < traces/trace64.din
./dineroIV -l1-usize 16k -l1-ubsize 16 -l1-uassoc 1 -informat d < traces/trace64.din
./dineroIV -l1-usize 32k -l1-ubsize 16 -l1-uassoc 1 -informat d < traces/trace64.din
./dineroIV -l1-usize 64k -l1-ubsize 16 -l1-uassoc 1 -informat d < traces/trace64.din
./dineroIV -l1-usize 128k -l1-ubsize 16 -l1-uassoc 1 -informat d < traces/trace64.din
./dineroIV -l1-usize 1k -l1-ubsize 16 -l1-uassoc 2 -informat d < traces/trace64.din
./dineroIV -l1-usize 2k -l1-ubsize 16 -l1-uassoc 2 -informat d < traces/trace64.din
./dineroIV -l1-usize 4k -l1-ubsize 16 -l1-uassoc 2 -informat d < traces/trace64.din
./dineroIV -l1-usize 8k -l1-ubsize 16 -l1-uassoc 2 -informat d < traces/trace64.din
./dineroIV -l1-usize 16k -l1-ubsize 16 -l1-uassoc 2 -informat d < traces/trace64.din
./dineroIV -l1-usize 32k -l1-ubsize 16 -l1-uassoc 2 -informat d < traces/trace64.din
./dineroIV -l1-usize 64k -l1-ubsize 16 -l1-uassoc 2 -informat d < traces/trace64.din
./dineroIV -l1-usize 128k -l1-ubsize 16 -l1-uassoc 2 -informat d < traces/trace64.din
```

## 3.

As can be seen in the spreadsheet, the only fraction that had any decrease was conflict, this is shown below and is likely due to how no matter the type of cache you are going to miss a lot when empty or full, but conflicts can be reduced by our cache organization and policies, such as, going from a direct mapped cache to a set associative cache.

![](q3.png)

DineroIV commands/script:
```sh
./dineroIV -l1-usize 1k -l1-ubsize 16 -l1-uassoc 1 -l1-uccc -informat d < traces/cc1.din
./dineroIV -l1-usize 2k -l1-ubsize 16 -l1-uassoc 1 -l1-uccc -informat d < traces/cc1.din
./dineroIV -l1-usize 4k -l1-ubsize 16 -l1-uassoc 1 -l1-uccc -informat d < traces/cc1.din
./dineroIV -l1-usize 8k -l1-ubsize 16 -l1-uassoc 1 -l1-uccc -informat d < traces/cc1.din
./dineroIV -l1-usize 16k -l1-ubsize 16 -l1-uassoc 1 -l1-uccc -informat d < traces/cc1.din
./dineroIV -l1-usize 32k -l1-ubsize 16 -l1-uassoc 1 -l1-uccc -informat d < traces/cc1.din
./dineroIV -l1-usize 64k -l1-ubsize 16 -l1-uassoc 1 -l1-uccc -informat d < traces/cc1.din
./dineroIV -l1-usize 128k -l1-ubsize 16 -l1-uassoc 1 -l1-uccc -informat d < traces/cc1.din
./dineroIV -l1-usize 1k -l1-ubsize 16 -l1-uassoc 2 -l1-uccc -informat d < traces/cc1.din
./dineroIV -l1-usize 2k -l1-ubsize 16 -l1-uassoc 2 -l1-uccc -informat d < traces/cc1.din
./dineroIV -l1-usize 4k -l1-ubsize 16 -l1-uassoc 2 -l1-uccc -informat d < traces/cc1.din
./dineroIV -l1-usize 8k -l1-ubsize 16 -l1-uassoc 2 -l1-uccc -informat d < traces/cc1.din
./dineroIV -l1-usize 16k -l1-ubsize 16 -l1-uassoc 2 -l1-uccc -informat d < traces/cc1.din
./dineroIV -l1-usize 32k -l1-ubsize 16 -l1-uassoc 2 -l1-uccc -informat d < traces/cc1.din
./dineroIV -l1-usize 64k -l1-ubsize 16 -l1-uassoc 2 -l1-uccc -informat d < traces/cc1.din
./dineroIV -l1-usize 128k -l1-ubsize 16 -l1-uassoc 2 -l1-uccc -informat d < traces/cc1.din
./dineroIV -l1-usize 1k -l1-ubsize 16 -l1-uassoc 1 -l1-uccc -informat d < traces/trace64.din
./dineroIV -l1-usize 2k -l1-ubsize 16 -l1-uassoc 1 -l1-uccc -informat d < traces/trace64.din
./dineroIV -l1-usize 4k -l1-ubsize 16 -l1-uassoc 1 -l1-uccc -informat d < traces/trace64.din
./dineroIV -l1-usize 8k -l1-ubsize 16 -l1-uassoc 1 -l1-uccc -informat d < traces/trace64.din
./dineroIV -l1-usize 16k -l1-ubsize 16 -l1-uassoc 1 -l1-uccc -informat d < traces/trace64.din
./dineroIV -l1-usize 32k -l1-ubsize 16 -l1-uassoc 1 -l1-uccc -informat d < traces/trace64.din
./dineroIV -l1-usize 64k -l1-ubsize 16 -l1-uassoc 1 -l1-uccc -informat d < traces/trace64.din
./dineroIV -l1-usize 128k -l1-ubsize 16 -l1-uassoc 1 -l1-uccc -informat d < traces/trace64.din
./dineroIV -l1-usize 1k -l1-ubsize 16 -l1-uassoc 2 -l1-uccc -informat d < traces/trace64.din
./dineroIV -l1-usize 2k -l1-ubsize 16 -l1-uassoc 2 -l1-uccc -informat d < traces/trace64.din
./dineroIV -l1-usize 4k -l1-ubsize 16 -l1-uassoc 2 -l1-uccc -informat d < traces/trace64.din
./dineroIV -l1-usize 8k -l1-ubsize 16 -l1-uassoc 2 -l1-uccc -informat d < traces/trace64.din
./dineroIV -l1-usize 16k -l1-ubsize 16 -l1-uassoc 2 -l1-uccc -informat d < traces/trace64.din
./dineroIV -l1-usize 32k -l1-ubsize 16 -l1-uassoc 2 -l1-uccc -informat d < traces/trace64.din
./dineroIV -l1-usize 64k -l1-ubsize 16 -l1-uassoc 2 -l1-uccc -informat d < traces/trace64.din
./dineroIV -l1-usize 128k -l1-ubsize 16 -l1-uassoc 2 -l1-uccc -informat d < traces/trace64.din
```

## 4.

The results did not differ substatially, in fact, they were all identical most likely due to the size of the L2 cache being to big to really see any difference.
![](q4.png)

DineroIV commands/script:
```sh
./dineroIV -l1-isize 16k -l1-ibsize 16 -l1-iassoc 1\
    -l1-dsize 16k -l1-dbsize 16 -l1-dassoc 2 -l1-dwback n -l1-dwalloc n\
    -l2-usize 2m -l2-ubsize 16 -l2-uassoc 125000\
    -informat d < traces/mtu-all.din
./dineroIV -l1-isize 16k -l1-ibsize 16 -l1-iassoc 1\
    -l1-dsize 16k -l1-dbsize 16 -l1-dassoc 2 -l1-dwback n -l1-dwalloc n\
    -l2-usize 2m -l2-ubsize 16 -l2-uassoc 125000 -l2-urepl f\
    -informat d < traces/mtu-all.din
./dineroIV -l1-isize 16k -l1-ibsize 16 -l1-iassoc 1\
    -l1-dsize 16k -l1-dbsize 16 -l1-dassoc 2 -l1-dwback n -l1-dwalloc n\
    -l2-usize 2m -l2-ubsize 16 -l2-uassoc 125000 -l2-urepl r\
    -informat d < traces/mtu-all.din
```