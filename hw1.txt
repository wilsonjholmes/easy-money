---Dinero IV cache simulator, version 7
---Written by Jan Edler and Mark D. Hill
---Copyright (C) 1997 NEC Research Institute, Inc. and Mark D. Hill.
---All rights reserved.
---Copyright (C) 1985, 1989 Mark D. Hill.  All rights reserved.
---See -copyright option for details

---Summary of options (-help option gives usage information).

-l1-usize 65536
-l1-ubsize 8
-l1-usbsize 8
-l1-uassoc 1
-l1-urepl l
-l1-ufetch d
-l1-uwalloc a
-l1-uwback a
-l1-uccc
-skipcount 0
-flushcount 0
-maxcount 0
-stat-interval 0
-informat d
-on-trigger 0x0
-off-trigger 0x0

---Simulation begins.
---Simulation complete.
l1-ucache
 Metrics		      Total	      Instrn	       Data	       Read	      Write	       Misc
 -----------------	      ------	      ------	      ------	      ------	      ------	      ------
 Demand Fetches		     1832478	     1380073	      452405	      281354	      171051	           0
  Fraction of total	      1.0000	      0.7531	      0.2469	      0.1535	      0.0933	      0.0000

 Demand Misses		       15263	        6426	        8837	        3341	        5496	           0
  Demand miss rate	      0.0083	      0.0047	      0.0195	      0.0119	      0.0321	      0.0000
   Compulsory misses	       11779	        4761	        7018	        2081	        4937	           0
   Capacity misses	           0	           0	           0	           0	           0	           0
   Conflict misses	        3484	        1665	        1819	        1260	         559	           0
   Compulsory fraction	      0.7717	      0.7409	      0.7942	      0.6229	      0.8983	      0.0000
   Capacity fraction	      0.0000	      0.0000	      0.0000	      0.0000	      0.0000	      0.0000
   Conflict fraction	      0.2283	      0.2591	      0.2058	      0.3771	      0.1017	      0.0000

 Multi-block refs                 0
 Bytes From Memory	      122104
 ( / Demand Fetches)	      0.0666
 Bytes To Memory	       48600
 ( / Demand Writes)	      0.2841
 Total Bytes r/w Mem	      170704
 ( / Demand Fetches)	      0.0932

---Execution complete.
---Dinero IV cache simulator, version 7
---Written by Jan Edler and Mark D. Hill
---Copyright (C) 1997 NEC Research Institute, Inc. and Mark D. Hill.
---All rights reserved.
---Copyright (C) 1985, 1989 Mark D. Hill.  All rights reserved.
---See -copyright option for details

---Summary of options (-help option gives usage information).

-l1-usize 65536
-l1-ubsize 8
-l1-usbsize 8
-l1-uassoc 2
-l1-urepl l
-l1-ufetch d
-l1-uwalloc a
-l1-uwback a
-l1-uccc
-skipcount 0
-flushcount 0
-maxcount 0
-stat-interval 0
-informat d
-on-trigger 0x0
-off-trigger 0x0

---Simulation begins.
---Simulation complete.
l1-ucache
 Metrics		      Total	      Instrn	       Data	       Read	      Write	       Misc
 -----------------	      ------	      ------	      ------	      ------	      ------	      ------
 Demand Fetches		     1832478	     1380073	      452405	      281354	      171051	           0
  Fraction of total	      1.0000	      0.7531	      0.2469	      0.1535	      0.0933	      0.0000

 Demand Misses		       12425	        5107	        7318	        2244	        5074	           0
  Demand miss rate	      0.0068	      0.0037	      0.0162	      0.0080	      0.0297	      0.0000
   Compulsory misses	       11779	        4761	        7018	        2081	        4937	           0
   Capacity misses	           0	           0	           0	           0	           0	           0
   Conflict misses	         646	         346	         300	         163	         137	           0
   Compulsory fraction	      0.9480	      0.9322	      0.9590	      0.9274	      0.9730	      0.0000
   Capacity fraction	      0.0000	      0.0000	      0.0000	      0.0000	      0.0000	      0.0000
   Conflict fraction	      0.0520	      0.0678	      0.0410	      0.0726	      0.0270	      0.0000

 Multi-block refs                 0
 Bytes From Memory	       99400
 ( / Demand Fetches)	      0.0542
 Bytes To Memory	       43528
 ( / Demand Writes)	      0.2545
 Total Bytes r/w Mem	      142928
 ( / Demand Fetches)	      0.0780

---Execution complete.
---Dinero IV cache simulator, version 7
---Written by Jan Edler and Mark D. Hill
---Copyright (C) 1997 NEC Research Institute, Inc. and Mark D. Hill.
---All rights reserved.
---Copyright (C) 1985, 1989 Mark D. Hill.  All rights reserved.
---See -copyright option for details

---Summary of options (-help option gives usage information).

-l1-usize 65536
-l1-ubsize 8
-l1-usbsize 8
-l1-uassoc 4
-l1-urepl l
-l1-ufetch d
-l1-uwalloc a
-l1-uwback a
-l1-uccc
-skipcount 0
-flushcount 0
-maxcount 0
-stat-interval 0
-informat d
-on-trigger 0x0
-off-trigger 0x0

---Simulation begins.
---Simulation complete.
l1-ucache
 Metrics		      Total	      Instrn	       Data	       Read	      Write	       Misc
 -----------------	      ------	      ------	      ------	      ------	      ------	      ------
 Demand Fetches		     1832478	     1380073	      452405	      281354	      171051	           0
  Fraction of total	      1.0000	      0.7531	      0.2469	      0.1535	      0.0933	      0.0000

 Demand Misses		       12008	        4858	        7150	        2124	        5026	           0
  Demand miss rate	      0.0066	      0.0035	      0.0158	      0.0075	      0.0294	      0.0000
   Compulsory misses	       11779	        4761	        7018	        2081	        4937	           0
   Capacity misses	           0	           0	           0	           0	           0	           0
   Conflict misses	         229	          97	         132	          43	          89	           0
   Compulsory fraction	      0.9809	      0.9800	      0.9815	      0.9798	      0.9823	      0.0000
   Capacity fraction	      0.0000	      0.0000	      0.0000	      0.0000	      0.0000	      0.0000
   Conflict fraction	      0.0191	      0.0200	      0.0185	      0.0202	      0.0177	      0.0000

 Multi-block refs                 0
 Bytes From Memory	       96064
 ( / Demand Fetches)	      0.0524
 Bytes To Memory	       43024
 ( / Demand Writes)	      0.2515
 Total Bytes r/w Mem	      139088
 ( / Demand Fetches)	      0.0759

---Execution complete.
---Dinero IV cache simulator, version 7
---Written by Jan Edler and Mark D. Hill
---Copyright (C) 1997 NEC Research Institute, Inc. and Mark D. Hill.
---All rights reserved.
---Copyright (C) 1985, 1989 Mark D. Hill.  All rights reserved.
---See -copyright option for details

---Summary of options (-help option gives usage information).

-l1-usize 65536
-l1-ubsize 8
-l1-usbsize 8
-l1-uassoc 8
-l1-urepl l
-l1-ufetch d
-l1-uwalloc a
-l1-uwback a
-l1-uccc
-skipcount 0
-flushcount 0
-maxcount 0
-stat-interval 0
-informat d
-on-trigger 0x0
-off-trigger 0x0

---Simulation begins.
---Simulation complete.
l1-ucache
 Metrics		      Total	      Instrn	       Data	       Read	      Write	       Misc
 -----------------	      ------	      ------	      ------	      ------	      ------	      ------
 Demand Fetches		     1832478	     1380073	      452405	      281354	      171051	           0
  Fraction of total	      1.0000	      0.7531	      0.2469	      0.1535	      0.0933	      0.0000

 Demand Misses		       11944	        4789	        7155	        2098	        5057	           0
  Demand miss rate	      0.0065	      0.0035	      0.0158	      0.0075	      0.0296	      0.0000
   Compulsory misses	       11779	        4761	        7018	        2081	        4937	           0
   Capacity misses	           0	           0	           0	           0	           0	           0
   Conflict misses	         165	          28	         137	          17	         120	           0
   Compulsory fraction	      0.9862	      0.9942	      0.9809	      0.9919	      0.9763	      0.0000
   Capacity fraction	      0.0000	      0.0000	      0.0000	      0.0000	      0.0000	      0.0000
   Conflict fraction	      0.0138	      0.0058	      0.0191	      0.0081	      0.0237	      0.0000

 Multi-block refs                 0
 Bytes From Memory	       95552
 ( / Demand Fetches)	      0.0521
 Bytes To Memory	       42928
 ( / Demand Writes)	      0.2510
 Total Bytes r/w Mem	      138480
 ( / Demand Fetches)	      0.0756

---Execution complete.
